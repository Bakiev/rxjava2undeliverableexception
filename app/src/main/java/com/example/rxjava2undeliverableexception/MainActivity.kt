package com.example.rxjava2undeliverableexception

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {
    private lateinit var disposable: Disposable
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
//        disposable = Flowable.range(0, 1_000_000)
//            .zipWith(Flowable.interval(200, TimeUnit.MILLISECONDS), BiFunction<Int, Long, Int> { t1, t2 -> t1 })
//            .subscribeOn(Schedulers.io())
//            .subscribe { value -> Log.d("MainActivity", value.toString()) }
        disposable = Single.fromCallable(this::longOperation)
            .subscribeOn(Schedulers.io())
            .subscribe()
    }

    override fun onStop() {
        super.onStop()
        disposable.dispose()
    }

    private fun longOperation() {

        for (n in 0 until 1_000_000) {
            Log.d("MainActivity", n.toString())
            Thread.sleep(10)
        }
    }
}
